//
//  DrawnWordView.h
//  KanjiConnect
//
//  Created by Ray Price on 5/3/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JapaneseWord.h"
#import "KanjiEntry.h"

@interface DrawnWordView : UIView {
    JapaneseWord *wordEntry;
}

-(void)setWord:(JapaneseWord*)inWord;
-(CGRect)getFrameForKanji:(KanjiEntry*)inKanjiEntry;

@property (nonatomic, retain) UILabel *meaningLabel;
@property (nonatomic, retain) UILabel *pronunciationLabel;
@property (nonatomic, retain) NSMutableArray *kanjiDrawings;

@end
