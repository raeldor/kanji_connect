//
//  AppDelegate.m
//  KanjiConnect
//
//  Created by Ray Price on 4/14/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "KcAppDelegate.h"

@implementation KcAppDelegate

@synthesize localOptions;
@synthesize appOptions;
@synthesize isLocalOptionsLoaded;
@synthesize isOptionsLoaded;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    // set notification callback for options
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localOptionsLoaded:) name:@"Local Options Loaded" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appOptionsLoaded:) name:@"AppOptions.plist Options Loaded" object:nil];
    
    // first load local options
    localOptions = [[KcLocalOptions alloc] init];
    
    return YES;
}

-(void)localOptionsLoaded:(NSNotification *) notification {
    // check for cloud access
    NSURL *ubiq = [[NSFileManager defaultManager]
                   URLForUbiquityContainerIdentifier:nil];
    if (ubiq) {
        NSLog(@"iCloud access at %@", ubiq);
        // if cloud is now found, start using if we were not before
        if (![KcLocalOptions getSingleton].isUsingCloud) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Enabled" message:@"iCloud has become available.  Switching over to using iCloud.  If you would prefer to work locally, please go to control panel and disable iCloud for this device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            // mark us as using cloud now
            [KcLocalOptions getSingleton].isUsingCloud = YES;
            [[KcLocalOptions getSingleton] save];
        }
    } else {
        NSLog(@"No iCloud access");
        // if we were previously using cloud, error and exit
        if ([KcLocalOptions getSingleton].isUsingCloud) {
            // show error and give option to restart locally or exit
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No iCloud" message:@"iCloud is not available.  Please try again later or, if you turned it off on purpose, you can choose to work local.  If problem persists, please contact support@rakudasoft.com." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:@"Work Local", nil];
            alert.delegate = self;
            [alert show];
            [alert release];
        }
        else {
            // otherwise, just show warning that cloud is good :S
            if (![KcLocalOptions getSingleton].cloudWarningShown) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No iCloud" message:@"iCloud is not available.  Consider enabling iCloud if you want to synchronize your progress between devices." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                // set error shown in options
                [KcLocalOptions getSingleton].cloudWarningShown = YES;
                [[KcLocalOptions getSingleton] save];
            }
        }
    }
    
    // local options are loaded
    isLocalOptionsLoaded = YES;
    
    // load app options and check for changes
    appOptions = [[KcAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
    [appOptions checkForCloudChanges]; // this sets up the query to get back the options
    
    
    NSLog(@"Finished loading local options");
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Local Options Loaded" object:nil];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // which dialog?
    if ([alertView.title isEqualToString:@"No iCloud"]) {
        // if user asked to work local
        if (buttonIndex == 2) {
            // turn off icloud and warning
            [KcLocalOptions getSingleton].isUsingCloud = NO;
            [KcLocalOptions getSingleton].cloudWarningShown = YES;
            [[KcLocalOptions getSingleton] save];
        }
    }
}

-(void)appOptionsLoaded:(NSNotification *) notification {
    NSLog(@"Finished loading app options");
    
    /*
   	// if database version has changed
	if (![localOptions.lastVersionNumber isEqualToString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]]) {
        // unpack decks
        [self copyBundledDocumentsOfType:@"flashDeck"];
        
        // update version number
        [localOptions setLastVersionNumber:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        [localOptions save];
	}*/
    
    // mark as loaded
    isOptionsLoaded = YES;
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppOptions.plist Options Loaded" object:nil];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
