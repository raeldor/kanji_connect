//
//  GuessKanjiFromMeaningViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectGame.h"
#import "QuestionViewControllerProtocol.h"
#import "KanjiDrawingView.h"

@interface GuessKanjiFromMeaningViewController : UIViewController <QuestionViewControllerProtocol> {
    IBOutlet UIButton *nextButton;
    
    IBOutlet UILabel *meaningLabel;
    
    IBOutlet UILabel *meaningLabel1;
    IBOutlet UILabel *meaningLabel2;
    IBOutlet UILabel *meaningLabel3;
    IBOutlet UILabel *meaningLabel4;
    
    IBOutlet UILabel *maruBatsuLabel1;
    IBOutlet UILabel *maruBatsuLabel2;
    IBOutlet UILabel *maruBatsuLabel3;
    IBOutlet UILabel *maruBatsuLabel4;
    
    IBOutlet KanjiDrawingView *kanjiDrawing1;
    IBOutlet KanjiDrawingView *kanjiDrawing2;
    IBOutlet KanjiDrawingView *kanjiDrawing3;
    IBOutlet KanjiDrawingView *kanjiDrawing4;
    
    ConnectGame *myGame;
}

-(IBAction)answerButtonTouched:(id)sender;
-(IBAction)nextButtonTouched:(id)sender;

@end
