//
//  ConnectGame.h
//  KanjiConnect
//
//  Created by Ray Price on 4/14/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlashGameTypes.h"
#import "JapaneseDictionary.h"
#import "KanjiDictionary.h"
#import "ConnectGameLink.h"
#import "ConnectGameObject.h"
#import "KanjiRadical.h"

@interface ConnectGame : NSObject {
    // store all kanjis for grade level
    NSMutableArray *allKanjis;
    
    NSMutableArray *availableGameTypes; // gameTypeEnum
    
    JapaneseDictionary *japaneseDictionary;
    KanjiDictionary *kanjiDictionary;

    NSMutableArray *currentPossibleAnswers;
    KanjiRadical *missingRadical;
    
}

@property (nonatomic, retain) ConnectGameLink *currentLink;
@property (nonatomic, retain) ConnectGameLink *nextLink;

-(id)init;

// used by view controller for gameplay
-(BOOL)setupQuestionUsingCurrentLink;
-(gameTypeEnum)getGameType;
-(NSArray*)getPossibleAnswers;
-(NSObject*)getQuestionDictionaryEntry;
-(dictionaryTypeEnum)getQuestionDictionaryEntryType;
-(NSObject*)getViaDictionaryEntry;
-(KanjiRadical*)getMissingRadical;
-(dictionaryTypeEnum)getViaDictionaryEntryType;
-(void)markQuestionRight;
-(void)markQuestionWrong;

// used to move on
-(BOOL)generateNextLink;
-(BOOL)goToNextLink;

// helper functions
-(KanjiRadical*)pickRadicalFromList:(NSArray*)inList thatIsNot:(KanjiRadical*)inRadical;
-(NSArray*)getKanjiSimilarTo:(KanjiEntry*)inKanji maxReturnCount:(int)inMaxCount;
-(NSArray*)getMeaningUniqueWordsUsingKanji:(KanjiEntry*)inKanji thatIsNotWord:(JapaneseWord*)inNotWord forJlptLevel:(jlptLevelEnum)inJlptLevel maxReturnCount:(int)inMaxCount;
-(ConnectGameLink*)getLinkForLowestDeckObjectForJlptLevel:(jlptLevelEnum)inJlptLevel andGradeLevel:(gradeLevelEnum)inGradeLevel;

@end



