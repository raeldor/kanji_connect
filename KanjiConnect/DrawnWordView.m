//
//  DrawnWordView.m
//  KanjiConnect
//
//  Created by Ray Price on 5/3/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "DrawnWordView.h"
#import "KanjiDictionary.h"
#import "DrawingAndMeaningView.h"

@implementation DrawnWordView

@synthesize meaningLabel;
@synthesize pronunciationLabel;
@synthesize kanjiDrawings;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        kanjiDrawings = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)awakeFromNib{
    // custom init code
    kanjiDrawings = [[NSMutableArray alloc] init];
}

-(void)setWord:(JapaneseWord*)inWord {
    // remove any existing sub views
    while (kanjiDrawings.count > 0) {
        DrawingAndMeaningView *thisDrawing = [kanjiDrawings objectAtIndex:0];
        [thisDrawing removeFromSuperview];
        [kanjiDrawings removeObjectAtIndex:0];
    }
    
    // set background to clear in case we set to something else so it shows up
    // in interface builder
    self.backgroundColor = [UIColor clearColor];
    
    // save a link to the word
    wordEntry = [inWord retain];
    
    // set background to white in case we set to something else so it shows up
    // in interface builder
    self.backgroundColor = [UIColor whiteColor];
    
    // save 15% at top for furigana
    double kanjiHeight = self.frame.size.height*0.70f;
    double kanjiWidth = self.frame.size.height*(0.70f*0.8f); // to make square
    CGSize kanjiSize = CGSizeMake(kanjiWidth, kanjiHeight);
    
    // loop through kanji and create drawing objects
    double nextStartPosition = (self.frame.size.width-(kanjiWidth*(double)inWord.kanji.length))/2.0f;
    for (int i=0; i < inWord.kanji.length; i++) {
        // get this character
        NSString *thisKanji = [inWord.kanji substringWithRange:NSMakeRange(i, 1)];
        
        // get kanji from database
        KanjiEntry *thisEntry = [[KanjiDictionary getSingleton] getEntryUsingKanji:thisKanji];
        
        // create drawing
        CGRect thisFrame = CGRectMake(nextStartPosition, self.frame.size.height*0.15f, kanjiSize.width, kanjiSize.height);
        DrawingAndMeaningView *newDrawing = [[DrawingAndMeaningView alloc] initWithFrame:thisFrame];
        newDrawing.drawingView.drawing = thisEntry.drawing;
        newDrawing.meaningLabel.text = [thisEntry.meanings objectAtIndex:0];
        [self addSubview:newDrawing];
        [kanjiDrawings addObject:newDrawing];
        [newDrawing release];
        
        // hide meaning by default
        newDrawing.meaningLabel.hidden = YES;
        
        // increment start position
        nextStartPosition += kanjiSize.width;
    }
    
    // add label for pronuciation at top
    CGRect pronunciationFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height*0.15f);
    pronunciationLabel = [[UILabel alloc] initWithFrame:pronunciationFrame];
    pronunciationLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:pronunciationLabel];
    
    // add label for meaning at bottom
    CGRect meaningFrame = CGRectMake(0, self.frame.size.height*0.85f, self.frame.size.width, self.frame.size.height*0.15f);
    meaningLabel = [[UILabel alloc] initWithFrame:meaningFrame];
    meaningLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:meaningLabel];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(CGRect)getFrameForKanji:(KanjiEntry*)inKanjiEntry {
    // find where in the word this kanji is
    double kanjiWidth = self.frame.size.height*(0.70f*0.8f); // to make square
    double nextStartPosition = (self.frame.size.width-(kanjiWidth*(double)wordEntry.kanji.length))/2.0f;
    for (int i=0; i < wordEntry.kanji.length; i++) {
        // get this character
        NSString *thisKanji = [wordEntry.kanji substringWithRange:NSMakeRange(i, 1)];
        if ([thisKanji isEqualToString:inKanjiEntry.kanji]) {
            return CGRectMake(nextStartPosition, self.frame.size.height*0.15f, kanjiWidth, self.frame.size.height*0.70);
            break;
        }
        nextStartPosition += kanjiWidth;
    }
    return CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
}

-(void)dealloc {
    // release ours retains
    [wordEntry release];
    [kanjiDrawings release];
    [meaningLabel release];
    [pronunciationLabel release];
    
    // dealloc super
    [super dealloc];
}

@end
