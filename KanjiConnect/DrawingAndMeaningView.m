//
//  DrawingAndMeaningView.m
//  KanjiConnect
//
//  Created by Ray Price on 5/3/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "DrawingAndMeaningView.h"

@implementation DrawingAndMeaningView

@synthesize drawingView;
@synthesize meaningLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        // set background to clear in case we set to something else so it shows up
        // in interface builder
        self.backgroundColor = [UIColor clearColor];
        
        // add sub-view for drawing, allow 20% for label
        CGRect drawingFrame = CGRectMake(0, 0, frame.size.width, frame.size.height*0.8f);
        CGRect meaningFrame = CGRectMake(0, 0+frame.size.height*0.8f, frame.size.width, frame.size.height*0.2f);
        
        // create drawing and add as sub-view
        drawingView = [[KanjiDrawingView alloc] initWithFrame:drawingFrame];
        drawingView.isStatic = YES;
        drawingView.keepAspect = NO;
        drawingView.userInteractionEnabled = NO;
        drawingView.drawColor = [UIColor blackColor];
        drawingView.shadowColor = [UIColor lightGrayColor];
        drawingView.strokeWidth = drawingView.strokeWidth * 0.5f;
        drawingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:drawingView];
        
        // create label and add as sub-view
        meaningLabel = [[UILabel alloc] initWithFrame:meaningFrame];
        meaningLabel.textAlignment = NSTextAlignmentCenter;
        meaningLabel.text = @"";
//        meaningLabel.font = [meaningLabel.font fontWithSize:48.0f];
        meaningLabel.adjustsFontSizeToFitWidth = YES;
        meaningLabel.minimumScaleFactor = 0.1f;
//        meaningLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight;*/
        [self addSubview:meaningLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)dealloc {
    // release out variables
    [drawingView release];
    [meaningLabel release];
    
    // dealloc super
    [super dealloc];
}

@end
