//
//  ConnectGameLink.h
//  KanjiConnect
//
//  Created by Ray Price on 4/18/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlashGameTypes.h"
#import "JapaneseDictionary.h"
#import "KanjiDictionary.h"
#import "ConnectGameObject.h"
#import "FlashCard.h"

@interface ConnectGameLink : NSObject {
    int linkToleitnerDeck; // just for debugging!
}

@property (nonatomic, retain) ConnectGameObject *linkFromObject;
@property (nonatomic, retain) ConnectGameObject *linkViaObject;
@property (nonatomic, retain) ConnectGameObject *linkToObject;
@property (nonatomic, assign) gameTypeEnum linkToGameType;

-(id)initWithFromObject:(ConnectGameObject*)inFromObject toObject:(ConnectGameObject*)inToObject viaObject:(ConnectGameObject*)inViaObject toGameType:(gameTypeEnum)inGameType;
-(NSComparisonResult)leitnerCompare:(ConnectGameObject*)otherObject;
-(int)getLinkToLeitnerDeck;
-(NSString*)getLinkToCustomCategory;
-(FlashCard*)createFlashCardFromToLink;
-(NSString*)getToLinkTypeAsSectionTypeString;
-(void)markToLinkAsAnsweredRight;
-(void)markToLinkAsAnsweredWrong;

@end
