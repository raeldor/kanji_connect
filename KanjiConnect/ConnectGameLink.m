//
//  ConnectGameLink.m
//  KanjiConnect
//
//  Created by Ray Price on 4/18/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "ConnectGameLink.h"
#import "KcAppOptions.h"
#import "FlashCard.h"
#import "ConnectGame.h"

@implementation ConnectGameLink

@synthesize linkFromObject;
@synthesize linkViaObject;
@synthesize linkToObject;
@synthesize linkToGameType;

-(id)initWithFromObject:(ConnectGameObject*)inFromObject toObject:(ConnectGameObject*)inToObject viaObject:(ConnectGameObject*)inViaObject toGameType:(gameTypeEnum)inGameType {
    self = [super init];
    if (self) {
        // save inputs
        linkFromObject = [inFromObject retain];
        linkToObject = [inToObject retain];
        linkViaObject = [inViaObject retain];
        linkToGameType = inGameType;
    }
    return self;
}

-(FlashCard*)createFlashCardFromToLink {
    // create a temporary flash card
    // translate to object type to string
    FlashCard *tempCard = nil;
    NSString *typeString = @"Unknown";
    switch (linkToObject.objectType) {
        case dictionaryType_Word:
            typeString = @"Dictionary";
            tempCard = [[FlashCard alloc] initWithKanji:((JapaneseWord*)linkToObject.dictionaryObject).kanji kana:((JapaneseWord*)linkToObject.dictionaryObject).kana senseIndex:1];
            break;
        case dictionaryType_Kanji:
            tempCard = [[FlashCard alloc] initWithKanji:((KanjiEntry*)linkToObject.dictionaryObject).kanji kana:nil meanings:nil notes:nil];
            typeString = @"Kanji";
            break;
        default:
            break;
    }
    
    return [tempCard autorelease];
}

-(NSString*)getToLinkTypeAsSectionTypeString {
    // get type string
    NSString *typeString = @"Unknown";
    switch (linkToObject.objectType) {
        case dictionaryType_Word:
            typeString = @"Dictionary";
            break;
        case dictionaryType_Kanji:
            typeString = @"Kanji";
            break;
        default:
            break;
    }
    return typeString;
}

// get leitner deck for link to
-(int)getLinkToLeitnerDeck {
    // create a temporary flash card and get section type
    FlashCard *tempCard = [self createFlashCardFromToLink];
    NSString *typeString = [self getToLinkTypeAsSectionTypeString];
    
    // get leitner deck
    int leitnerDeck = [[KcAppOptions getSingleton] getLeitnerDeckNumberForCard:tempCard ofType:typeString withGameType:linkToGameType];
    
    // for debugging only!
    linkToleitnerDeck = leitnerDeck;
    
    return leitnerDeck;
}

-(void)markToLinkAsAnsweredRight {
    // create a temporary flash card and get section type
    FlashCard *card = [self createFlashCardFromToLink];
    NSString *typeString = [self getToLinkTypeAsSectionTypeString];
    
    // find out when card was last shown
    KcAppOptions *appOptions = [KcAppOptions getSingleton];
    int leitnerDeck = [appOptions getLeitnerDeckNumberForCard:card ofType:typeString withGameType:linkToGameType];
    int daysSinceShown = [appOptions getDaysSinceLastShownForCard:card ofType:typeString withGameType:linkToGameType];
    int minutesSinceShown = [appOptions getMinutesSinceLastShownForCard:card ofType:typeString withGameType:linkToGameType];
    
    int oldDeck = leitnerDeck;
    
    // if you're seeing it after you're due to, and still get it right, then put it in the
    // deck that you should have seen it in, ie if you're supposed to see it after a day
    // but it's actually been a week, put it in the week deck first before decrementing
    if (minutesSinceShown > 5 && leitnerDeck < -2)
        leitnerDeck = -2;
    if (minutesSinceShown > 15 && leitnerDeck < -1)
        leitnerDeck = -1;
    if (minutesSinceShown > 60 && leitnerDeck < 0)
        leitnerDeck = 0;
    if (daysSinceShown > 1 && leitnerDeck < 1)
        leitnerDeck = 1;
    if (daysSinceShown > 7 && leitnerDeck < 2)
        leitnerDeck = 2;
    if (daysSinceShown > 30 && leitnerDeck < 3)
        leitnerDeck = 3;
    if (daysSinceShown > 30*3 && leitnerDeck < 4)
        leitnerDeck = 4;
    if (daysSinceShown > 30*6 && leitnerDeck < 5)
        leitnerDeck = 5;
    if (daysSinceShown > 30*9 && leitnerDeck < 6)
        leitnerDeck = 6;
    if (daysSinceShown > 365 && leitnerDeck < 7)
        leitnerDeck = 7;
    [appOptions setLeitnerDeckNumberForCard:card ofType:typeString withGameType:linkToGameType toValue:leitnerDeck autoSave:NO];
    
    // NOW increment leitner deck
    [appOptions incrementLeitnerDeckNumberForCard:card ofType:typeString withGameType:linkToGameType autoSave:NO];
    
    // also update last shown
    [appOptions updateLastShownTimestampForCard:card ofType:typeString withGameType:linkToGameType autoSave:YES];
    
    int newDeck = [appOptions getLeitnerDeckNumberForCard:card ofType:typeString withGameType:linkToGameType];
    NSLog(@"Went from deck %d to deck %d", oldDeck, newDeck);
}

-(void)markToLinkAsAnsweredWrong {
    // create a temporary flash card and get section type
    FlashCard *tempCard = [self createFlashCardFromToLink];
    NSString *typeString = [self getToLinkTypeAsSectionTypeString];
    
    // mark as wrong
    [[KcAppOptions getSingleton] decrementLeitnerDeckNumberForCard:tempCard ofType:typeString withGameType:linkToGameType autoSave:NO];
    
    // mark as shown
    [[KcAppOptions getSingleton] updateLastShownTimestampForCard:tempCard ofType:typeString withGameType:linkToGameType autoSave:YES];
}

-(NSString*)getLinkToCustomCategory {
    // get custom category as string for object
    NSString *customCategory = nil;
    if (linkToObject.objectType == dictionaryType_Kanji)
        customCategory = ((JapaneseWord*)linkToObject.dictionaryObject).customCategory;
    else {
        if (linkToObject.objectType == dictionaryType_Radical)
            customCategory = ((KanjiEntry*)linkToObject.dictionaryObject).customCategory;
        else {
            if (linkToObject.objectType == dictionaryType_Radical)
                customCategory = ((KanjiEntry*)linkToObject.dictionaryObjectParent).customCategory;
        }
    }
    return customCategory;
}

// allow sorting of objects by leitner deck
-(NSComparisonResult)leitnerCompare:(ConnectGameLink*)otherObject {
    // get leitner decks
    int leitnerDeck = [self getLinkToLeitnerDeck];
    int otherLeitnerDeck = [otherObject getLinkToLeitnerDeck];
    
    // sort by custom category descending within leitner
    NSString *customCategory = [self getLinkToCustomCategory];
    NSString *otherCustomCategory = [otherObject getLinkToCustomCategory];
    
    // return compare result
    if (leitnerDeck > otherLeitnerDeck)
        return NSOrderedDescending;
    else {
        if (leitnerDeck < otherLeitnerDeck)
            return NSOrderedAscending;
        else {
            if ([customCategory compare:otherCustomCategory] == NSOrderedAscending)
                return NSOrderedAscending;
            else {
                if ([customCategory compare:otherCustomCategory] == NSOrderedDescending)
                    return NSOrderedDescending;
                else
                    return NSOrderedSame;
            }
        }
    }
}

-(void)dealloc {
    // release our retains
    [linkFromObject release];
    [linkViaObject release];
    [linkToObject release];
    
    // release super
    [super dealloc];
}

@end
