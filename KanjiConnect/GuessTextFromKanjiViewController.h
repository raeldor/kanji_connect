//
//  GuessTextFromKanjiViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectGame.h"
#import "QuestionViewControllerProtocol.h"
#import "KanjiDrawingView.h"

@interface GuessTextFromKanjiViewController : UIViewController <QuestionViewControllerProtocol> {
    IBOutlet UIButton *nextButton;
    
    IBOutlet KanjiDrawingView *questionKanjiDrawing;
    IBOutlet UILabel *answerLabel1;
    IBOutlet UILabel *answerLabel2;
    IBOutlet UILabel *answerLabel3;
    IBOutlet UILabel *answerLabel4;
    
    IBOutlet UILabel *maruBatsuLabel1;
    IBOutlet UILabel *maruBatsuLabel2;
    IBOutlet UILabel *maruBatsuLabel3;
    IBOutlet UILabel *maruBatsuLabel4;
    
    ConnectGame *myGame;
}

-(IBAction)answerButtonTouched:(id)sender;
-(IBAction)nextButtonTouched:(id)sender;

@end
