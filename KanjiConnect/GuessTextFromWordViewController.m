//
//  GuessTextFromTextViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "GuessTextFromWordViewController.h"
#import "KcViewController.h"
#import "DrawingAndMeaningView.h"

@interface GuessTextFromWordViewController ()

@end

@implementation GuessTextFromWordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setConnectGame:(ConnectGame *)inGame {
    // save game
    myGame = inGame;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    // clear congrats label and hide next button
    nextButton.hidden = YES;

    // get answer entry
    JapaneseWord *correctEntry = (JapaneseWord*)[myGame getQuestionDictionaryEntry];
        
    // set word kanji in the middle
    [questionWord setWord:correctEntry];
    
    // set meaning labels from kanji
    answerLabel1.text = @"";
    answerLabel2.text = @"";
    answerLabel3.text = @"";
    answerLabel4.text = @"";
    maruBatsuLabel1.text = @"";
    maruBatsuLabel2.text = @"";
    maruBatsuLabel3.text = @"";
    maruBatsuLabel4.text = @"";
    NSArray *possibles = [myGame getPossibleAnswers];
    for (int i=0; i < possibles.count; i++) {
        // get entry
        JapaneseWord *thisEntry = [possibles objectAtIndex:i];
        
        // what is this answer
        NSString *thisAnswer = @"";
        if ([myGame getGameType] == gameType_GuessMeaningFromKanji)
            thisAnswer = [thisEntry.englishMeanings objectAtIndex:0];
        else {
            if ([myGame getGameType] == gameType_GuessKanjiFromMeaning)
                thisAnswer = thisEntry.kanji;
            else
                if ([myGame getGameType] == gameType_GuessKanaFromKanji)
                    thisAnswer = thisEntry.kana;
        }
        
        // update drawing
        switch (i) {
            case 0:
                answerLabel1.text = thisAnswer;
                break;
            case 1:
                answerLabel2.text = thisAnswer;
                break;
            case 2:
                answerLabel3.text = thisAnswer;
                break;
            case 3:
                answerLabel4.text = thisAnswer;
                break;
            default:
                break;
        }
    }
    
    // call super
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)answerButtonTouched:(id)sender {
    UIButton *touchedButton = (UIButton*)sender;
    NSArray *possibles = [myGame getPossibleAnswers];
    JapaneseWord *touchedEntry = [possibles objectAtIndex:touchedButton.tag];
    JapaneseWord *correctEntry = (JapaneseWord*)[myGame getQuestionDictionaryEntry];
    if (touchedEntry == correctEntry) {
        // mark as correct
        [myGame markQuestionRight];
    }
    else {
        // mark as incorrect
        [myGame markQuestionWrong];
    }
    
    // show the correct meanings of all answers
    for (int i=0; i < possibles.count; i++) {
        // show answer
        JapaneseWord *thisEntry = [possibles objectAtIndex:i];
        switch (i) {
            case 0:
                if (thisEntry == correctEntry) {
                    maruBatsuLabel1.text = @"O";
                    maruBatsuLabel1.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel1.text = @"X";
                        maruBatsuLabel1.textColor = [UIColor redColor];
                    }
                }
                break;
            case 1:
                if (thisEntry == correctEntry) {
                    maruBatsuLabel2.text = @"O";
                    maruBatsuLabel2.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel2.text = @"X";
                        maruBatsuLabel2.textColor = [UIColor redColor];
                    }
                }
                break;
            case 2:
                if (thisEntry == correctEntry) {
                    maruBatsuLabel3.text = @"O";
                    maruBatsuLabel3.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel3.text = @"X";
                        maruBatsuLabel3.textColor = [UIColor redColor];
                    }
                }
                break;
            case 3:
                if (thisEntry == correctEntry) {
                    maruBatsuLabel4.text = @"O";
                    maruBatsuLabel4.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel4.text = @"X";
                        maruBatsuLabel4.textColor = [UIColor redColor];
                    }
                }
                break;
            default:
                break;
        }
    }
    
    // show pronunciation
    questionWord.meaningLabel.text = [correctEntry.englishMeanings objectAtIndex:0];
    questionWord.pronunciationLabel.text = correctEntry.kana;
    
    // show meanings of all kanjis
    for (int i=0; i < questionWord.kanjiDrawings.count; i++) {
        DrawingAndMeaningView *thisDrawing = [questionWord.kanjiDrawings objectAtIndex:i];
        thisDrawing.meaningLabel.font = [thisDrawing.meaningLabel.font fontWithSize:8];
        thisDrawing.meaningLabel.hidden = NO;
    }
    
    // show next button
    nextButton.hidden = NO;
}

-(IBAction)nextButtonTouched:(id)sender {
    // get parent view controller
    KcViewController *parentVc = (KcViewController*)self.parentViewController;
    
    // generate next link and find out the link
    [parentVc generateNextLink];
    
    // inform parent view controller of the frame reference where that link is used
    // in this answer, then it can display the link kanji/radical over the top
    [parentVc startLinkAnimationUsingFrame:[self getFrameForLinkObject:myGame.nextLink.linkViaObject]];
    
    // fade now
    [UIView animateWithDuration:0.5f animations:^{
        // fade out
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // remove from parent view controller and view
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

    // pop this view controller
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

-(CGRect)getFrameForLinkObject:(ConnectGameObject*)inLinkObject {
    // get parent view controller
    KcViewController *parentVc = (KcViewController*)self.parentViewController;
    
    // get linked from kanji
    KanjiEntry *linkedFromKanji = (KanjiEntry*)inLinkObject.dictionaryObject;
    if (inLinkObject.objectType == dictionaryType_Radical)
        linkedFromKanji = (KanjiEntry*)inLinkObject.dictionaryObjectParent;
    
    // get kanji frame from word drawing
    CGRect kanjiFrame = [questionWord getFrameForKanji:linkedFromKanji];
    CGRect linkFrame = [questionWord convertRect:kanjiFrame toView:parentVc.view];
    
    return linkFrame;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
