//
//  KcLocalOptions.h
//  KanjiConnect
//
//  Created by Ray Price on 4/30/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFile.h"
#import "DataFileDelegate.h"
#import "FlashLocalOptions.h"

@interface KcLocalOptions : FlashLocalOptions {
}

+(KcLocalOptions*)getSingleton;
-(id)init;
-(void)dealloc;

@property (nonatomic, assign) NSString *lastVersionNumber;

@end
