//
//  ConnectGameObject.h
//  KanjiConnect
//
//  Created by Ray Price on 4/22/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlashGameTypes.h"
#import "JapaneseDictionary.h"

@interface ConnectGameObject : NSObject {
}

-(id)initWithDictionaryObject:(NSObject*)inDictionaryObject ofType:(dictionaryTypeEnum)inObjectType;

@property (nonatomic, assign) dictionaryTypeEnum objectType;
@property (nonatomic, retain) NSObject *dictionaryObject;
@property (nonatomic, retain) NSObject *dictionaryObjectParent; // kanji if radical

-(int)getObjectId;

@end
