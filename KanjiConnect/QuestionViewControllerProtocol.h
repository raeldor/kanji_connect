//
//  QuestionViewControllerProtocol.h
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#ifndef KanjiConnect_QuestionViewControllerProtocol_h
#define KanjiConnect_QuestionViewControllerProtocol_h

#import "ConnectGame.h"

@protocol QuestionViewControllerProtocol

-(void)setConnectGame:(ConnectGame*)inGame;
-(CGRect)getFrameForLinkObject:(ConnectGameObject*)inLinkObject;

@optional

@end

#endif
