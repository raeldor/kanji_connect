//
//  ViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 4/14/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectGame.h"
#import "KanjiDrawingView.h"
#import "BuildKanjiView.h"
#import "QuestionViewControllerProtocol.h"

@interface KcViewController : UIViewController {
    ConnectGame *myGame;
    IBOutlet KanjiDrawingView *linkDrawing;
    IBOutlet UILabel *linkMeaning;
    
    IBOutlet UIView *containerView;

    KanjiDrawingView *linkAnimationDrawing;
    
    BOOL isFirstQuestion;
}

-(IBAction)nextButtonTouched:(id)sender;

-(void)generateNextLink;
-(void)goToNextQuestion;
-(UIViewController<QuestionViewControllerProtocol>*)pushControllerForQuestion;
-(void)refreshQuestionFromGame;
-(void)startLinkAnimationUsingFrame:(CGRect)inFrame;

@end
