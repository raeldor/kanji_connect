//
//  DrawingAndMeaningView.h
//  KanjiConnect
//
//  Created by Ray Price on 5/3/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDrawingView.h"

@interface DrawingAndMeaningView : UIView {
    KanjiDrawingView *drawingView;
    UILabel *meaningLabel;
}

@property (nonatomic, retain) KanjiDrawingView *drawingView;
@property (nonatomic, retain) UILabel *meaningLabel;

@end
