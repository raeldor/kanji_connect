//
//  ViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 4/14/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "KcViewController.h"
#import "JapaneseDictionary.h"
#import "KanjiRadical.h"
#import "DrawMissingRadicalViewController.h"
#import "KcAppDelegate.h"
#import "GuessTextFromWordViewController.h"
#import "GuessKanjiFromMeaningViewController.h"
#import "DrawingAndMeaningView.h"

@interface KcViewController ()

@end

@implementation KcViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // temporary to force linker to use this class
    Class test = [KanjiDrawingView class];
    Class test2 = [BuildKanjiView class];
    
    // setup drawing view
    linkDrawing.isStatic = YES;
    linkDrawing.keepAspect = NO;
    linkDrawing.userInteractionEnabled = NO;
    linkDrawing.drawColor = [UIColor blackColor];
    linkDrawing.shadowColor = [UIColor lightGrayColor];
    linkDrawing.strokeWidth = linkDrawing.strokeWidth * 0.25f;
    
    // register for options loaded event
    // we can't create the game until we have app options
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appOptionsLoaded) name:@"AppOptions.plist Options Loaded" object:nil];
    
    // first question, don't go to next
    isFirstQuestion = YES;
}

-(void)appOptionsLoaded {
    // now we can initialize the game
    myGame = [[ConnectGame alloc] init];
    
    // first question?
    if (isFirstQuestion)
        isFirstQuestion = NO;
    else {
        // go to the next question
        [myGame generateNextLink];
        [myGame goToNextLink];
    }
    
    // push view controller for game
    [self pushControllerForQuestion];
}

-(void)generateNextLink {
    // generate link and set as next
    [myGame generateNextLink];
}

-(void)goToNextQuestion {
    // go to the next question
    [myGame goToNextLink];
    
    // push view controller for game
    [self pushControllerForQuestion];
}

-(void)startLinkAnimationUsingFrame:(CGRect)inFrame {
    // add 20% to frame for meaning
    CGRect fullFrame = CGRectMake(inFrame.origin.x, inFrame.origin.y, inFrame.size.width, inFrame.size.height*(1.0f/0.8f));
    
    // find the link drawing and (if it's a radical, what it's a part of)
    DrawingAndMeaningView *newDrawing = [[DrawingAndMeaningView alloc] initWithFrame:fullFrame];
    KanjiEntry *kanjiLink;
    KanjiRadical *radicalLink = nil;
    if (myGame.nextLink.linkViaObject.objectType == dictionaryType_Kanji)
        kanjiLink = (KanjiEntry*)myGame.nextLink.linkViaObject.dictionaryObject;
    else {
        // must be radical, so find parent kanji
        kanjiLink = (KanjiEntry*)myGame.nextLink.linkViaObject.dictionaryObjectParent;
        radicalLink = (KanjiRadical*)myGame.nextLink.linkViaObject.dictionaryObject;
    }
    newDrawing.drawingView.drawing = kanjiLink.drawing;
    newDrawing.meaningLabel.text = [kanjiLink.meanings objectAtIndex:0];
    [self.view addSubview:newDrawing];
    
    // create radical drawing here, so scaling is the same
    DrawingAndMeaningView *radicalDrawing = nil;
    if (radicalLink != nil) {
        radicalDrawing = [[DrawingAndMeaningView alloc] initWithFrame:newDrawing.frame];
        radicalDrawing.drawingView.drawing = radicalLink.drawing;
        radicalDrawing.drawingView.partOfDrawing = kanjiLink.drawing;
        radicalDrawing.meaningLabel.text = [radicalLink.meanings objectAtIndex:0];
        radicalDrawing.alpha = 0.0f;
        [self.view addSubview:radicalDrawing];
    }
    
    // fade out container
    [UIView animateWithDuration:0.5f animations:^{
        containerView.alpha = 0.0f;
    }];
    
    // if it's a radical that's part of a kanji, move the kanji to the center, then fade
    // all but the radical we're using
    [UIView animateWithDuration:0.5f delay:0.5f options:UIViewAnimationOptionCurveEaseIn animations:^{
        newDrawing.center = self.view.center;
        newDrawing.transform = CGAffineTransformMakeScale(220.0f/newDrawing.frame.size.width, (220.0f*(1.0f/0.8f))/newDrawing.frame.size.height);
        newDrawing.meaningLabel.alpha = 1.0f;
        radicalDrawing.center = self.view.center;
        radicalDrawing.transform = newDrawing.transform;
    } completion:^(BOOL finished) {
        // if link via object is a radical, fade to radical
        float pushDelay = 0.5f;
        if (myGame.nextLink.linkViaObject.objectType == dictionaryType_Radical) {
            // create new drawing with just radical and fade between
            [UIView animateWithDuration:0.5f delay:0.5f options:UIViewAnimationOptionCurveEaseIn animations:^{
                // replace kanji drawing with radical
                radicalDrawing.alpha = 1.0f;
                newDrawing.alpha = 0.0f;
            } completion:^(BOOL finished) {
            }];
            
            // make delay longer
            pushDelay += 1.0f;
        }
        
        // don't show new controller we're pushing yet!
        containerView.alpha = 0.0f;
        
        // go to next link and push new controller
        [self goToNextQuestion];
        UIViewController<QuestionViewControllerProtocol> *questionController = [self pushControllerForQuestion];
        
        // ask new controller for position of linked from kanji
        CGRect linkRect = [questionController getFrameForLinkObject:myGame.currentLink.linkViaObject];
        
        [UIView animateWithDuration:0.5f delay:pushDelay options:UIViewAnimationOptionCurveEaseIn animations:^{
            // move kanji to same position as view controller
            newDrawing.center = CGPointMake(linkRect.origin.x+linkRect.size.width/2.0f, linkRect.origin.y+(linkRect.size.height*(1.0f/0.8f))/2.0f);
            CGSize realRect = CGSizeApplyAffineTransform(newDrawing.frame.size, CGAffineTransformInvert(newDrawing.transform));
            float scaleX = linkRect.size.width/realRect.width;
            float scaleY = (linkRect.size.height*(1.0f/0.8f))/realRect.height;
            newDrawing.transform = CGAffineTransformMakeScale(scaleX, scaleY);
            radicalDrawing.center = newDrawing.center;
            radicalDrawing.transform = newDrawing.transform;
        } completion:^(BOOL finished) {
            // fade from radical/kanji to view
            [UIView animateWithDuration:0.5f animations:^{
                newDrawing.alpha = 0.0f;
                radicalDrawing.alpha = 0.0f;
                containerView.alpha = 1.0f;
            } completion:^(BOOL finished) {
                // don't need the animation of the link anymore
                [newDrawing removeFromSuperview];
                [radicalDrawing removeFromSuperview];
            }];
        }];
    }];
}

-(void)viewDidAppear:(BOOL)animated {
    // make sure options are loaded!
    KcAppDelegate *appDelegate = (KcAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isOptionsLoaded) {
        // first question?
        if (isFirstQuestion)
            isFirstQuestion = NO;
        else {
            // go to the next question
            [myGame generateNextLink];
            [myGame goToNextLink];
        }
        
        // push view controller for game
        [self pushControllerForQuestion];
    }
}

-(UIViewController<QuestionViewControllerProtocol>*)pushControllerForQuestion {
    // set link drawing
    if ([myGame getViaDictionaryEntryType] == dictionaryType_Kanji) {
        linkDrawing.drawing = ((KanjiEntry*)[myGame getViaDictionaryEntry]).drawing;
        linkMeaning.text = [((KanjiEntry*)[myGame getViaDictionaryEntry]).meanings objectAtIndex:0];
    }
    else {
        if ([myGame getViaDictionaryEntryType] == dictionaryType_Radical) {
            linkDrawing.drawing = ((KanjiRadical*)[myGame getViaDictionaryEntry]).drawing;
            linkMeaning.text = [((KanjiRadical*)[myGame getViaDictionaryEntry]).meanings objectAtIndex:0];
        }
        else {
        }
        if ([myGame getViaDictionaryEntryType] == dictionaryType_Word) {
        }
    }
    linkDrawing.quantizedDrawing = nil;
    [linkDrawing refreshDrawing];
    
    // get view controller to add
    GuessKanjiFromMeaningViewController *newController = nil;
    
    // draw missing radical
    if ([myGame getGameType] == gameType_WriteMissingRadical) {
        // instantiate controller
        newController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"WriteMissingRadical"];
        
        // perform segue
//        [self performSegueWithIdentifier:@"WriteMissingRadicalSegue" sender:nil];
    }
    
    // guess meaning from kanji for a word
    if ([myGame getGameType] == gameType_GuessMeaningFromKanji && [myGame getQuestionDictionaryEntryType] == dictionaryType_Word) {
        // instantiate controller
        newController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"GuessTextFromWord"];
        
        // perform segue
//        [self performSegueWithIdentifier:@"GuessTextFromTextSegue" sender:nil];
    }
    
    // guess kanji from meaning for kanji
    if ([myGame getGameType] == gameType_GuessKanjiFromMeaning && [myGame getQuestionDictionaryEntryType] == dictionaryType_Kanji) {
        // instantiate controller
        newController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"GuessKanjiFromMeaning"];
        
        // perform segue
//        [self performSegueWithIdentifier:@"GuessKanjiFromMeaningSegue" sender:nil];
    }
    
    // guess kanji from meaning for a kanji
    if ([myGame getQuestionDictionaryEntryType] == dictionaryType_Kanji &&
        [myGame getGameType] == gameType_GuessMeaningFromKanji) {
        // instantiate controller
        newController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"GuessTextFromKanji"];
        
        // perform segue
//        [self performSegueWithIdentifier:@"GuessTextFromKanjiSegue" sender:nil];
    }
    
    // guess kanji from meaning for a word
    if ([myGame getQuestionDictionaryEntryType] == dictionaryType_Word && (
        [myGame getGameType] == gameType_GuessKanjiFromMeaning ||
        [myGame getGameType] == gameType_GuessKanaFromKanji)) {
        // instantiate controller
        newController = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"GuessTextFromWord"];
        
        // perform segue
//        [self performSegueWithIdentifier:@"GuessTextFromTextSegue" sender:nil];
    }
    
    // add view controller
    if (newController != nil) {
        [newController setConnectGame:myGame];
        [self addChildViewController:newController];
        [containerView addSubview:newController.view];
    }
    else {
        int n=0;
    }
    
    return newController;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
    
    // set the game
//    [[segue destinationViewController] setConnectGame:myGame];
}

-(void)refreshQuestionFromGame {    
    // set link drawing
    if ([myGame getViaDictionaryEntryType] == dictionaryType_Kanji) {
        linkDrawing.drawing = ((KanjiEntry*)[myGame getViaDictionaryEntry]).drawing;
        linkMeaning.text = [((KanjiEntry*)[myGame getViaDictionaryEntry]).meanings objectAtIndex:0];
    }
    else {
        linkDrawing.drawing = ((KanjiRadical*)[myGame getViaDictionaryEntry]).drawing;
        linkMeaning.text = [((KanjiRadical*)[myGame getViaDictionaryEntry]).meanings objectAtIndex:0];
    }
    linkDrawing.quantizedDrawing = nil;
    [linkDrawing refreshDrawing];
    
    // guess kanji from meaning for a kanji
    if ([myGame getGameType] == gameType_GuessKanjiFromMeaning && [myGame getQuestionDictionaryEntryType] == dictionaryType_Kanji) {
    }
}

-(IBAction)nextButtonTouched:(id)sender {
    // set up next link
    [myGame generateNextLink];
    
    // go to next link
    [myGame goToNextLink];
    
    // refresh question
    [self refreshQuestionFromGame];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    // free our variables
    [myGame release];
    
    // release super
    [super dealloc];
}

@end
