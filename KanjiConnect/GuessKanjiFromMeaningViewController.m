//
//  GuessKanjiFromMeaningViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "GuessKanjiFromMeaningViewController.h"
#import "KcViewController.h"

@interface GuessKanjiFromMeaningViewController ()

@end

@implementation GuessKanjiFromMeaningViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setConnectGame:(ConnectGame *)inGame {
    // save game
    myGame = inGame;
    
    // using the via link, tell the parent view controller the position of the link object
    // in the new screen so we can move it before we fade in this view
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // intialize kanji drawings
    [kanjiDrawing1 initialize];
    [kanjiDrawing2 initialize];
    [kanjiDrawing3 initialize];
    [kanjiDrawing4 initialize];
    
    // setup static drawings
    kanjiDrawing1.isStatic = YES;
    kanjiDrawing1.keepAspect = NO;
    kanjiDrawing1.userInteractionEnabled = NO;
    kanjiDrawing1.drawColor = [UIColor blackColor];
    kanjiDrawing1.shadowColor = [UIColor lightGrayColor];
    kanjiDrawing1.strokeWidth = kanjiDrawing1.strokeWidth * 0.5f;
    kanjiDrawing2.isStatic = YES;
    kanjiDrawing2.keepAspect = NO;
    kanjiDrawing2.userInteractionEnabled = NO;
    kanjiDrawing2.drawColor = [UIColor blackColor];
    kanjiDrawing2.shadowColor = [UIColor lightGrayColor];
    kanjiDrawing2.strokeWidth = kanjiDrawing2.strokeWidth * 0.5f;
    kanjiDrawing3.isStatic = YES;
    kanjiDrawing3.keepAspect = NO;
    kanjiDrawing3.userInteractionEnabled = NO;
    kanjiDrawing3.drawColor = [UIColor blackColor];
    kanjiDrawing3.shadowColor = [UIColor lightGrayColor];
    kanjiDrawing3.strokeWidth = kanjiDrawing3.strokeWidth * 0.5f;
    kanjiDrawing4.isStatic = YES;
    kanjiDrawing4.keepAspect = NO;
    kanjiDrawing4.userInteractionEnabled = NO;
    kanjiDrawing4.drawColor = [UIColor blackColor];
    kanjiDrawing4.shadowColor = [UIColor lightGrayColor];
    kanjiDrawing4.strokeWidth = kanjiDrawing4.strokeWidth * 0.5f;
}

-(void)viewWillAppear:(BOOL)animated {
    // hide next button
    nextButton.hidden = YES;
    
    // set meaning
    KanjiEntry *correctEntry = (KanjiEntry*)[myGame getQuestionDictionaryEntry];
    NSString *meaningString = @"";
    for (int i=0; i < correctEntry.meanings.count && i < 1; i++) {
        NSString *thisMeaning = [correctEntry.meanings objectAtIndex:i];
        meaningString = [NSString stringWithFormat:@"%@%@,", meaningString, thisMeaning];
    }
    meaningString = [meaningString substringToIndex:meaningString.length-1];
    meaningLabel.text = meaningString;
    
    // clear existing drawings
    kanjiDrawing1.drawing = [[KanjiDrawing alloc] init];
    kanjiDrawing2.drawing = [[KanjiDrawing alloc] init];
    kanjiDrawing3.drawing = [[KanjiDrawing alloc] init];
    kanjiDrawing4.drawing = [[KanjiDrawing alloc] init];
    kanjiDrawing1.quantizedDrawing = nil;
    kanjiDrawing2.quantizedDrawing = nil;
    kanjiDrawing3.quantizedDrawing = nil;
    kanjiDrawing4.quantizedDrawing = nil;
    meaningLabel1.text = @"";
    meaningLabel2.text = @"";
    meaningLabel3.text = @"";
    meaningLabel4.text = @"";
    maruBatsuLabel1.text = @"";
    maruBatsuLabel2.text = @"";
    maruBatsuLabel3.text = @"";
    maruBatsuLabel4.text = @"";
    
    // set kanji drawings
    NSArray *possibles = [myGame getPossibleAnswers];
    for (int i=0; i < possibles.count; i++) {
        // get entry
        KanjiEntry *thisEntry = [possibles objectAtIndex:i];
        
        // update drawing
        switch (i) {
            case 0:
                kanjiDrawing1.drawing = thisEntry.drawing;
                break;
            case 1:
                kanjiDrawing2.drawing = thisEntry.drawing;
                break;
            case 2:
                kanjiDrawing3.drawing = thisEntry.drawing;
                break;
            case 3:
                kanjiDrawing4.drawing = thisEntry.drawing;
                break;
            default:
                break;
        }
    }
    
    // refresh all drawings
    [kanjiDrawing1 refreshDrawing];
    [kanjiDrawing2 refreshDrawing];
    [kanjiDrawing3 refreshDrawing];
    [kanjiDrawing4 refreshDrawing];

    // call super
    [super viewWillAppear:animated];
}

-(IBAction)answerButtonTouched:(id)sender {
    UIButton *touchedButton = (UIButton*)sender;
    NSArray *possibles = [myGame getPossibleAnswers];
    KanjiEntry *touchedEntry = [possibles objectAtIndex:touchedButton.tag];
    KanjiEntry *correctEntry = (KanjiEntry*)[myGame getQuestionDictionaryEntry];
    if (touchedEntry == correctEntry) {
        // mark as correct
        [myGame markQuestionRight];
    }
    else {
        // mark as correct
        [myGame markQuestionWrong];
    }
    
    // show the correct meanings of all answers
    for (int i=0; i < possibles.count; i++) {
        // show answer
        KanjiEntry *thisEntry = [possibles objectAtIndex:i];
        switch (i) {
            case 0:
                meaningLabel1.text = [thisEntry.meanings objectAtIndex:0];
                if (thisEntry == correctEntry) {
                    maruBatsuLabel1.text = @"O";
                    maruBatsuLabel1.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel1.text = @"X";
                        maruBatsuLabel1.textColor = [UIColor redColor];
                    }
                }
                break;
            case 1:
                meaningLabel2.text = [thisEntry.meanings objectAtIndex:0];
                if (thisEntry == correctEntry) {
                    maruBatsuLabel2.text = @"O";
                    maruBatsuLabel2.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel2.text = @"X";
                        maruBatsuLabel2.textColor = [UIColor redColor];
                    }
                }
                break;
            case 2:
                meaningLabel3.text = [thisEntry.meanings objectAtIndex:0];
                if (thisEntry == correctEntry) {
                    maruBatsuLabel3.text = @"O";
                    maruBatsuLabel3.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel3.text = @"X";
                        maruBatsuLabel3.textColor = [UIColor redColor];
                    }
                }
                break;
            case 3:
                meaningLabel4.text = [thisEntry.meanings objectAtIndex:0];
                if (thisEntry == correctEntry) {
                    maruBatsuLabel4.text = @"O";
                    maruBatsuLabel4.textColor = [UIColor greenColor];
                }
                else {
                    if (thisEntry == touchedEntry) {
                        maruBatsuLabel4.text = @"X";
                        maruBatsuLabel4.textColor = [UIColor redColor];
                    }
                }
                break;
            default:
                break;
        }
    }
    
    // show next button
    nextButton.hidden = NO;
}

-(CGRect)getFrameForLinkObject:(ConnectGameObject*)inLinkObject {
    // get parent view controller
    KcViewController *parentVc = (KcViewController*)self.parentViewController;
    
    // before we fade out
    CGRect fromFrame;
    NSArray *possibles = [myGame getPossibleAnswers];
    KanjiEntry *correctEntry;
    if (inLinkObject.objectType == dictionaryType_Radical)
        correctEntry = (KanjiEntry*)inLinkObject.dictionaryObjectParent;
    else
        correctEntry = (KanjiEntry*)inLinkObject.dictionaryObject;
    for (int i=0; i < possibles.count; i++) {
        KanjiEntry *thisEntry = [possibles objectAtIndex:i];
        if (thisEntry == correctEntry) {
            switch (i) {
                case 0:
                    fromFrame = [self.view convertRect:kanjiDrawing1.frame toView:parentVc.view];
                    break;
                case 1:
                    fromFrame = [self.view convertRect:kanjiDrawing2.frame toView:parentVc.view];
                    break;
                case 2:
                    fromFrame = [self.view convertRect:kanjiDrawing3.frame toView:parentVc.view];
                    break;
                case 3:
                    fromFrame = [self.view convertRect:kanjiDrawing4.frame toView:parentVc.view];
                    break;
                default:
                    break;
            }
            break;
        }
    }
    
    return fromFrame;
}

-(IBAction)nextButtonTouched:(id)sender {
    // get parent view controller
    KcViewController *parentVc = (KcViewController*)self.parentViewController;
    
    // generate next link and find out the link
    [parentVc generateNextLink];
    
    // inform parent view controller of the frame reference where that link is used
    // in this answer, then it can display the link kanji/radical over the top
    [parentVc startLinkAnimationUsingFrame:[self getFrameForLinkObject:myGame.nextLink.linkViaObject]];
    
    // fade now
    [UIView animateWithDuration:0.5f animations:^{
        // fade out
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // remove from parent view controller and view
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];

    // pop this view controller
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
