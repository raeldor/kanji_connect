//
//  DrawMissingRadicalViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "DrawMissingRadicalViewController.h"
#import "KcViewController.h"

@interface DrawMissingRadicalViewController ()

@end

@implementation DrawMissingRadicalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setConnectGame:(ConnectGame *)inGame {
    // save game
    myGame = inGame;
}

-(void)viewDidLoad
{
    // call super
    [super viewDidLoad];
    
    // configure static drawings
    missingRadicalDrawing.isStatic = YES;
    missingRadicalDrawing.keepAspect = NO;
    missingRadicalDrawing.userInteractionEnabled = NO;
    missingRadicalDrawing.drawColor = [UIColor blackColor];
    missingRadicalDrawing.shadowColor = [UIColor lightGrayColor];
    missingRadicalAnimateDrawing.isStatic = YES;
    missingRadicalAnimateDrawing.keepAspect = NO;
    missingRadicalAnimateDrawing.userInteractionEnabled = NO;
    missingRadicalAnimateDrawing.drawColor = [UIColor blackColor];
    missingRadicalAnimateDrawing.shadowColor = [UIColor lightGrayColor];
    
    // user draws on this!
    missingRadicalUserDrawing.isStatic = NO;
    missingRadicalUserDrawing.userInteractionEnabled = YES;
    missingRadicalUserDrawing.drawColor = [UIColor blackColor];
    missingRadicalUserDrawing.shadowColor = [UIColor lightGrayColor];
}

-(void)viewWillAppear:(BOOL)animated {
    // clear congrats label and hide next button
    congratsLabel.text = @"";
    nextButton.hidden = YES;
    
    // set meaning
    KanjiEntry *correctEntry = (KanjiEntry*)[myGame getQuestionDictionaryEntry];
    NSString *meaningString = @"";
    for (int i=0; i < correctEntry.meanings.count && i < 1; i++) {
        NSString *thisMeaning = [correctEntry.meanings objectAtIndex:i];
        meaningString = [NSString stringWithFormat:@"%@%@,", meaningString, thisMeaning];
    }
    meaningString = [meaningString substringToIndex:meaningString.length-1];
    missingRadicalKanjiMeaningLabel.text = meaningString;
    
    // draw kanji but not the missing radical
    // sometimes the radical occurs twice, so only miss drawing once!
    BOOL alreadyMissed = NO;
    NSMutableArray *myStrokes = [NSMutableArray array];
    KanjiRadical *missingRadical = ((KanjiRadical*)[myGame getMissingRadical]);
    for (int i=0; i < correctEntry.radicals.count; i++) {
        KanjiRadical *thisRadical = [correctEntry.radicals objectAtIndex:i];
        if (![thisRadical.kanji isEqualToString:missingRadical.kanji] || alreadyMissed) {
            [myStrokes addObjectsFromArray:thisRadical.drawing.strokes];
        }
        else
            alreadyMissed = YES;
    }
    
    // refresh drawing
    KanjiDrawing *newDrawing = [[KanjiDrawing alloc] initWithStrokes:myStrokes];
    missingRadicalDrawing.drawing = newDrawing;
    missingRadicalDrawing.partOfDrawing = correctEntry.drawing;
    [newDrawing release];
    [missingRadicalDrawing refreshDrawing];
    
    // clear animation
    missingRadicalAnimateDrawing.drawing = nil;
    [missingRadicalAnimateDrawing refreshDrawing];
    
    // allow user to draw
    missingRadicalUserDrawing.drawColor = [UIColor blackColor];
    missingRadicalUserDrawing.alpha = 1.0f;
    [missingRadicalUserDrawing clearStrokes];
    [missingRadicalDrawing refreshDrawing];
    [missingRadicalUserDrawing enableDrawing];
    
    // call super
    [super viewWillAppear:animated];
}

-(IBAction)clearButtonTouched:(id)sender {
	// if we have drawn any strokes, clear them first
	if (missingRadicalUserDrawing.drawing.strokes.count > 0) {
        // remove all strokes on long press
        [missingRadicalUserDrawing clearStrokes];
        [missingRadicalUserDrawing refreshDrawing];
        
        // remove quantized drawing too, otherwise the old version gets left behind
        // this was causing old drawing to be shown in gray when new draw was failing
        missingRadicalUserDrawing.quantizedDrawing = nil;
	}
}

-(IBAction)checkButtonTouched:(id)sender {
    // draw missing radical
    if ([myGame getGameType] == gameType_WriteMissingRadical) {
        // did we draw correctly
        double score = [[myGame getMissingRadical].drawing compareWithDrawing:missingRadicalUserDrawing.drawing];
        NSLog(@"Score was %f", score);
        if (score > 0.7f) {
            // show correct message
            congratsLabel.textColor = [UIColor greenColor];
            congratsLabel.text = @"Correct";
            
            // mark as correct
            [myGame markQuestionRight];
        }
        else {
            // mark as incorrect
            [myGame markQuestionWrong];
            
            // turn user drawing red and fade it
            [UIView animateWithDuration:0.5f animations:^{
                // show in red and fade if we have strokes
                if (missingRadicalUserDrawing.drawing.strokes.count > 0) {
                    missingRadicalUserDrawing.drawColor = [UIColor redColor];
                    [missingRadicalUserDrawing refreshDrawing];
                    missingRadicalUserDrawing.alpha = 0.25f;
                }
            } completion:^(BOOL finished) {
                // show animation of correct radical
                KanjiDrawing *newDrawing = [[KanjiDrawing alloc] initWithStrokes:[myGame getMissingRadical].drawing.strokes];
                missingRadicalAnimateDrawing.drawing = newDrawing;
                missingRadicalAnimateDrawing.partOfDrawing = ((KanjiEntry*)[myGame getQuestionDictionaryEntry]).drawing;
                [newDrawing release];
                [missingRadicalAnimateDrawing startPlay];
            }];
            
            
            // show wrong message
            congratsLabel.textColor = [UIColor orangeColor];
            congratsLabel.text = @"Incorrect";
        }
    }
    
    // show next button
    nextButton.hidden = NO;
}

-(IBAction)nextButtonTouched:(id)sender {
    // get parent view controller
    KcViewController *parentVc = (KcViewController*)self.parentViewController;
    
    // generate next link and find out the link
    [parentVc generateNextLink];
    
    // inform parent view controller of the frame reference where that link is used
    // in this answer, then it can display the link kanji/radical over the top
    [parentVc startLinkAnimationUsingFrame:[self getFrameForLinkObject:myGame.nextLink.linkViaObject]];
    
    // fade now
    [UIView animateWithDuration:0.5f animations:^{
        // fade out
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // remove from parent view controller and view
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
    // pop this view controller
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

-(CGRect)getFrameForLinkObject:(ConnectGameObject*)inLinkObject {
    // get parent view controller
    KcViewController *parentVc = (KcViewController*)self.parentViewController;
    
    // get kanji frame
    CGRect kanjiFrame = missingRadicalDrawing.frame;
    CGRect linkFrame = [self.view convertRect:kanjiFrame toView:parentVc.view];
    
    return linkFrame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
