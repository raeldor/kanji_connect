//
//  DrawMissingRadicalViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 4/29/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDrawingView.h"
#import "QuestionViewControllerProtocol.h"

@interface DrawMissingRadicalViewController : UIViewController <QuestionViewControllerProtocol>  {
    IBOutlet UILabel *congratsLabel;
    IBOutlet UIButton *nextButton;
    
    IBOutlet UILabel *missingRadicalKanjiMeaningLabel;
    IBOutlet KanjiDrawingView *missingRadicalDrawing;
    IBOutlet KanjiDrawingView *missingRadicalUserDrawing;
    IBOutlet KanjiDrawingView *missingRadicalAnimateDrawing;
    
    ConnectGame *myGame;
}

-(IBAction)checkButtonTouched:(id)sender;
-(IBAction)clearButtonTouched:(id)sender;
-(IBAction)nextButtonTouched:(id)sender;

@end
