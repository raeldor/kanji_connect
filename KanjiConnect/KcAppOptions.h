//
//  KcAppOptions.h
//  KanjiConnect
//
//  Created by Ray Price on 4/30/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "FlashAppOptions.h"

@interface KcAppOptions : FlashAppOptions {
    
}

+(KcAppOptions*)getSingleton;

@end
