//
//  ConnectGame.m
//  KanjiConnect
//
//  Created by Ray Price on 4/14/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "ConnectGame.h"
#import "FlashGame.h"
#import "TextFunctions.h"
#import "KcAppOptions.h"
#import "NSMutableArrayShuffle.h"

@implementation ConnectGame

@synthesize currentLink;
@synthesize nextLink;

-(id)init {
    // initialize
    self = [super init];
    if (self) {
        // save dictionaries for speed
        japaneseDictionary = [[JapaneseDictionary getSingleton] retain];
        kanjiDictionary = [[KanjiDictionary getSingleton] retain];
        
        // get list of all kanji for this grade level
        allKanjis = [[kanjiDictionary getKanjiListFromGradeLevel:gradeLevel_6] retain];
        
        // build list of available game types for each dictionary type
        
        availableGameTypes = [[NSMutableArray alloc] init];
        [availableGameTypes addObject:[NSNumber numberWithInteger:gameType_GuessKanjiFromMeaning]];
        [availableGameTypes addObject:[NSNumber numberWithInteger:gameType_GuessKanaFromKanji]];
        [availableGameTypes addObject:[NSNumber numberWithInteger:gameType_GuessMeaningFromKanji]];
        [availableGameTypes addObject:[NSNumber numberWithInteger:gameType_WriteKanjiFromMeaning]];
        [availableGameTypes addObject:[NSNumber numberWithInteger:gameType_GuessMissingRadical]];
        [availableGameTypes addObject:[NSNumber numberWithInteger:gameType_WriteMissingRadical]];
        
        // initialize arrays
        currentPossibleAnswers = [[NSMutableArray alloc] init];
        
        // to start the game, find the lowest card for the jlpt/grade level
        currentLink = [[self getLinkForLowestDeckObjectForJlptLevel:jlptLevel_N3 andGradeLevel:gradeLevel_6] retain];
        
        // setup question
        [self setupQuestionUsingCurrentLink];
    }
    return self;
}

-(BOOL)setupQuestionUsingCurrentLink {
    // create array to hold possible answers
    NSMutableArray *pickedPossibles = [NSMutableArray array];

    // write missing radical
    if (currentLink.linkToGameType == gameType_WriteMissingRadical) {
        // pick a radical for our kanji to be missing, but not the one we linked from since it
        // will be displayed at the top of the screen, lol
        [missingRadical release];
        KanjiRadical *pickedRadical = [self pickRadicalFromList:((KanjiEntry*)currentLink.linkToObject.dictionaryObject).radicals thatIsNot:(KanjiRadical*)currentLink.linkViaObject.dictionaryObject];
        
        // we should have found a radical, so set as missing
        if (pickedRadical != nil)
            missingRadical = [pickedRadical retain];
        else {
            missingRadical = nil;
        }
    }
    
    // guess kanji from meaning
    if (currentLink.linkToGameType == gameType_GuessKanjiFromMeaning ||
        currentLink.linkToGameType == gameType_GuessMeaningFromKanji ||
        currentLink.linkToGameType == gameType_GuessKanaFromKanji) {
        // if we are guessing a kanji
        if (currentLink.linkToObject.objectType == dictionaryType_Kanji) {
            // type cast object
            KanjiEntry *thisEntry = (KanjiEntry*)currentLink.linkToObject.dictionaryObject;
            
            // add this kanji to possible answers
            [pickedPossibles addObject:currentLink.linkToObject.dictionaryObject];
            
            // did we link from a radical or a word
            NSArray *otherKanji = nil;
            if (currentLink.linkViaObject.objectType == dictionaryType_Radical) {
                // get linked from radical
                KanjiRadical *thisRadical = (KanjiRadical*)currentLink.linkViaObject.dictionaryObject;
                
                // pick 3 other kanji that have same radical in same position within your
                // jlpt level
                otherKanji = [thisRadical getKanjiListUsingRadicalInSamePositionForGradeLevel:gradeLevel_5 thatIsNotKanji:thisEntry maxReturnCount:3 allowOutOfGradeLevel:YES];
            }
            else {
                // find other kanji that have radicals in the same position to find
                // meanings to compare against
                otherKanji = [self getKanjiSimilarTo:thisEntry maxReturnCount:3];
            }
            
            // if nothing was found, need to pick another kanji
            if (otherKanji == nil)
                return NO;
            
            // save other kanji to picked array
            for (int i=0; i < otherKanji.count; i++) {
                KanjiEntry *thisEntry =[otherKanji objectAtIndex:i];;
                [pickedPossibles addObject:thisEntry];
            }
        }
        
        // if we are guessing a word
        if (currentLink.linkToObject.objectType == dictionaryType_Word) {
            // type cast object
            JapaneseWord *thisEntry = (JapaneseWord*)currentLink.linkToObject.dictionaryObject;
            
            // add this kanji to possible answers
            [pickedPossibles addObject:currentLink.linkToObject.dictionaryObject];
            
            // get linked from radical
            KanjiEntry *fromKanji = (KanjiEntry*)currentLink.linkViaObject.dictionaryObject;
            
            // pick 3 other words that have same kanji in same position within your
            // jlpt level
            NSArray *otherWords = [self getMeaningUniqueWordsUsingKanji:fromKanji thatIsNotWord:thisEntry forJlptLevel:jlptLevel_N3 maxReturnCount:3];
            
            // if nothing was found, need to pick another kanji
            if (otherWords == nil)
                return NO;
            
            // save other kanji to picked array
            for (int i=0; i < otherWords.count; i++) {
                JapaneseWord *thisEntry =[otherWords objectAtIndex:i];;
                [pickedPossibles addObject:thisEntry];
            }
        }
    }
    
    // if we found possibles
    if (pickedPossibles.count > 0) {
        // shuffle possible answers
        [currentPossibleAnswers removeAllObjects];
        while (pickedPossibles.count > 0) {
            int r = pickedPossibles.count==1?0:arc4random()%pickedPossibles.count;
            [currentPossibleAnswers addObject:[pickedPossibles objectAtIndex:r]];
            [pickedPossibles removeObjectAtIndex:r];
        }
        
        // success
        return YES;
    }
    else
        // fail
        return NO;
}

-(gameTypeEnum)getGameType {
    return currentLink.linkToGameType;
}

-(NSArray*)getPossibleAnswers {
    return currentPossibleAnswers;
}

-(KanjiRadical*)getMissingRadical {
    return missingRadical;
}

-(NSObject*)getQuestionDictionaryEntry {
    return currentLink.linkToObject.dictionaryObject;
}

-(dictionaryTypeEnum)getQuestionDictionaryEntryType {
    return currentLink.linkToObject.objectType;
}

-(NSObject*)getViaDictionaryEntry {
    return currentLink.linkViaObject.dictionaryObject;
}

-(dictionaryTypeEnum)getViaDictionaryEntryType {
    return currentLink.linkViaObject.objectType;
}

-(BOOL)generateNextLink {
    // build a list of possible links and pick one at random
    NSMutableArray *possibleLinks = [NSMutableArray array];
    
    // if this is a kanji
    if (currentLink.linkToObject.objectType == dictionaryType_Kanji) {
        // get this entry
        KanjiEntry *thisKanji = (KanjiEntry*)currentLink.linkToObject.dictionaryObject;
        
        // always from the current object
        ConnectGameObject *fromObject = currentLink.linkToObject;
        
        // loop through radicals used in this kanji
        for (int i=0; i < thisKanji.radicals.count; i++) {
            // get this radical
            KanjiRadical *thisRadical = [thisKanji.radicals objectAtIndex:i];
            ConnectGameObject *viaObject = [[ConnectGameObject alloc] initWithDictionaryObject:thisRadical ofType:dictionaryType_Radical];
            viaObject.dictionaryObjectParent = thisKanji;
            
            // if this radical has a kanji (ie, a meaning)
            if (thisRadical.kanji.length > 0) {
                // see if we can use this radical to link to a new kanji
                // get kanji that use this radical
                NSMutableArray *kanjiList = [thisRadical getKanjiListUsingRadicalInSamePositionForGradeLevel:gradeLevel_5 thatIsNotKanji:thisKanji maxReturnCount:99 allowOutOfGradeLevel:NO];
                
                // if we have one, we can do a guess meaning from kanji for the kanji
                if (kanjiList.count > 1) {
                    // add a possibility for each kanji we find
                    for (int k=0; k < kanjiList.count; k++) {
                        // get this kanji
                        KanjiEntry *chosenKanji = [kanjiList objectAtIndex:k];
                        
                        // chosen kanji must have more than one radical, as we can't
                        // draw the one we linked from because the user can see it already!
                        if (chosenKanji.radicals.count > 0) {
                            // link to this kanji
                            ConnectGameObject *toObject = [[ConnectGameObject alloc] initWithDictionaryObject:chosenKanji ofType:dictionaryType_Kanji];
                            
                            // create a guess meaning from kanji game
                            // we can guess meaning from kanji here, since we're using
                            // the radical to link to a new kanji, so we don't know the kanji
                            // meaning yet
                            ConnectGameLink *kanjiLink;
                            kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessMeaningFromKanji];
//                            [possibleLinks addObject:kanjiLink];
                            [kanjiLink release];
                            
                            // create a guess kanji from meaning too
                            kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessKanjiFromMeaning];
//                            [possibleLinks addObject:kanjiLink];
                            [kanjiLink release];
                            
                            // create a write missing radical game too
                            kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_WriteMissingRadical];
                            [possibleLinks addObject:kanjiLink];
                            [kanjiLink release];
                            
                            // release objects
                            [toObject release];
                        }
                    }
                    
                    // we can use one of the kanji's radicals to play guess radical meaning
                }
            }
            
            // release via radical object
            [viaObject release];
        }
        
        // we can use the kanji to guess the meaning of a word that uses the kanji
        // check to see how many words use this kanji
        // find a word that uses this kanji that are within our study level
        NSArray *wordList = [thisKanji getRelatedWordListForJlptLevel:jlptLevel_N3 thatIsNotWord:nil maxReturnCount:99 allowOutOfGradeLevel:NO];
        if (wordList.count > 0) {
            // create a possibliity for each word found
            for (int w=0; w < wordList.count; w++) {
                // get word
                JapaneseWord *thisWord = [wordList objectAtIndex:w];
                
                // via object is the kanji
                ConnectGameObject *viaObject = [[ConnectGameObject alloc] initWithDictionaryObject:thisKanji ofType:dictionaryType_Kanji];
                
                // to object is this word
                ConnectGameObject *toObject = [[ConnectGameObject alloc] initWithDictionaryObject:thisWord ofType:dictionaryType_Word];
                
                // create a guess meaning from kanji game
                ConnectGameLink *kanjiLink;
                kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessMeaningFromKanji];
                [possibleLinks addObject:kanjiLink];
                [kanjiLink release];
                
                 // create a guess kana from kanji game for this word
                // a little too easy when it's a compound using kana!!!
                 kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessKanaFromKanji];
//                 [possibleLinks addObject:kanjiLink];
                 [kanjiLink release];
                
                // create a guess kanji from meaning too
                kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessKanjiFromMeaning];
// don't have this yet, because we don't have textfromtext controller
//                [possibleLinks addObject:kanjiLink];
                [kanjiLink release];
            }
        }
    }
    
    // if this is a word
    if (currentLink.linkToObject.objectType == dictionaryType_Word) {
        // get this entry
        JapaneseWord *thisWord = (JapaneseWord*)currentLink.linkToObject.dictionaryObject;
        KanjiEntry *linkedFromKanji = (KanjiEntry*)currentLink.linkFromObject.dictionaryObject;
        
        // we are linking from this word
        ConnectGameObject *fromObject = [[ConnectGameObject alloc] initWithDictionaryObject:thisWord ofType:dictionaryType_Word];
        
        // find the kanji used in this word that's within our grade level that is not
        // the kanji we linked from
        for (int i=0; i < thisWord.kanji.length; i++) {
            // if this is a kanji, but not the one we linked from
            NSString *kanjiString = [thisWord.kanji substringWithRange:NSMakeRange(i, 1)];
            KanjiEntry *chosenKanji = [kanjiDictionary getEntryUsingKanji:kanjiString];
            if (![TextFunctions isKana:kanjiString] && ![kanjiString isEqualToString:linkedFromKanji.kanji]) {
                // if there are other kanji that use the radicals from this kanji
                NSArray *similarKanji = [self getKanjiSimilarTo:chosenKanji maxReturnCount:3];
                if (similarKanji.count > 0) {
                    // link via kanji, as this is what we'll be animating
                    ConnectGameObject *viaObject = [[ConnectGameObject alloc] initWithDictionaryObject:chosenKanji ofType:dictionaryType_Kanji];
                    
                    // connect TO is kanji
                    ConnectGameObject *toObject = [[ConnectGameObject alloc] initWithDictionaryObject:chosenKanji ofType:dictionaryType_Kanji];
                    
                    // create a guess meaning from kanji game
                    // doesn't make sense if we're linking from kanji, since we'll already
                    // know it's meaning
                    /*
                    ConnectGameLink *kanjiLink;
                    kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessMeaningFromKanji];
                    [possibleLinks addObject:kanjiLink];
                    [kanjiLink release];*/
                    
                    // create a link to a radical game
                    
                    
                    // release objects
                    [viaObject release];
                }
                

                
                
                // loop through radicals used in this kanji
                for (int r=0; r < chosenKanji.radicals.count; r++) {
                    // get this radical
                    KanjiRadical *thisRadical = [chosenKanji.radicals objectAtIndex:r];
                    ConnectGameObject *viaObject = [[ConnectGameObject alloc] initWithDictionaryObject:thisRadical ofType:dictionaryType_Radical];
                    viaObject.dictionaryObjectParent = chosenKanji;
                    
                    // if this radical has a kanji (ie, a meaning)
                    if (thisRadical.kanji.length > 0) {
                        // see if we can use this radical to link to a new kanji
                        // get kanji that use this radical
                        NSMutableArray *kanjiList = [thisRadical getKanjiListUsingRadicalInSamePositionForGradeLevel:gradeLevel_5 thatIsNotKanji:chosenKanji maxReturnCount:99 allowOutOfGradeLevel:NO];
                        
                        // if we have one, we can do a guess meaning from kanji for the kanji
                        if (kanjiList.count > 1) {
                            // add a possibility for each kanji we find
                            for (int k=0; k < kanjiList.count; k++) {
                                // get this kanji
                                KanjiEntry *pickedKanji = [kanjiList objectAtIndex:k];
                                
                                // chosen kanji must have more than one radical, as we can't
                                // draw the one we linked from because the user can see it already!
                                if (pickedKanji.radicals.count > 0) {
                                    // link to this kanji
                                    ConnectGameObject *toObject = [[ConnectGameObject alloc] initWithDictionaryObject:pickedKanji ofType:dictionaryType_Kanji];
                                    
                                    // create a write missing radical game too
                                    ConnectGameLink *kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_WriteMissingRadical];
                                    [possibleLinks addObject:kanjiLink];
                                    [kanjiLink release];
                                    
                                    // release objects
                                    [toObject release];
                                }
                            }
                            
                            // we can use one of the kanji's radicals to play guess radical meaning
                        }
                    }
                }
                
                
                
                
            }
            
            // find words within our JLPT level that use this kanji
            NSArray *wordList = [chosenKanji getRelatedWordListForJlptLevel:jlptLevel_N3 thatIsNotWord:thisWord maxReturnCount:99 allowOutOfGradeLevel:NO];
            
            // if we found some words
            if (wordList.count > 0) {
                // create a possibility for each word
                for (int w=0; w < wordList.count; w++) {
                    // link via kanji, not word
                    ConnectGameObject *viaObject = [[ConnectGameObject alloc] initWithDictionaryObject:chosenKanji ofType:dictionaryType_Kanji];
                    
                    // connect TO is word
                    JapaneseWord *chosenWord = [wordList objectAtIndex:w];
                    ConnectGameObject *toObject = [[ConnectGameObject alloc] initWithDictionaryObject:chosenWord ofType:dictionaryType_Word];
                    
                    // create a guess meaning from kanji word game
                    ConnectGameLink *kanjiLink;
                    kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessMeaningFromKanji];
                    [possibleLinks addObject:kanjiLink];
                    [kanjiLink release];
                    
                    // create a guess kana from kanji word game
                    // a little too easy when it's a compound using kana
                    kanjiLink = [[ConnectGameLink alloc] initWithFromObject:fromObject toObject:toObject viaObject:viaObject toGameType:gameType_GuessKanaFromKanji];
//                    [possibleLinks addObject:kanjiLink];
                    [kanjiLink release];
                    
                    // release objects
                    [viaObject release];
                }
            }
        }
        
        // release objects
        [fromObject release];
        
    }
    
    // give them a shuffle before we sort, so we don't get all the games clustered
//    [possibleLinks shuffle];
    
    // pick a possible link at random and set as next link
    if (possibleLinks.count > 0) {
        // sort based on how well the user knows them and pick least well known
        NSArray *sortedArray = [possibleLinks sortedArrayUsingSelector:@selector(leitnerCompare:)];
        [nextLink release];
        nextLink = [[sortedArray objectAtIndex:0] retain];
    }
    else {
        // didn't find a link
        
        // find the lowest card of any game type
        nextLink = [[self getLinkForLowestDeckObjectForJlptLevel:jlptLevel_N3 andGradeLevel:gradeLevel_6] retain];
        
        return NO;
    }
    
    // success
    return YES;
}

-(NSArray*)getKanjiSimilarTo:(KanjiEntry*)inKanji maxReturnCount:(int)inMaxCount {
    // loop through radicals
    NSMutableArray *similarList = [NSMutableArray array];
    for (int r=0; r < inKanji.radicals.count; r++) {
        // get other kanji that have radicals in same position
        KanjiRadical *thisRadical = [inKanji.radicals objectAtIndex:r];
        NSArray *otherKanji = [thisRadical getKanjiListUsingRadicalInSamePositionForGradeLevel:gradeLevel_5 thatIsNotKanji:inKanji maxReturnCount:3 allowOutOfGradeLevel:YES];
        [similarList addObjectsFromArray:otherKanji];
    }
    
    // choose max
    NSMutableArray *chosenList = [NSMutableArray array];
    while (chosenList.count < inMaxCount && similarList.count > 0) {
        // pick random kanji
        int r = similarList.count==1?0:arc4random()%similarList.count;
        KanjiEntry *thisKanji = [similarList objectAtIndex:r];
        [chosenList addObject:thisKanji];
        [similarList removeObjectAtIndex:r];
    }
    
    return chosenList;
}

-(NSArray*)getMeaningUniqueWordsUsingKanji:(KanjiEntry*)inKanji thatIsNotWord:(JapaneseWord*)inNotWord forJlptLevel:(jlptLevelEnum)inJlptLevel maxReturnCount:(int)inMaxCount {
    // first get other words
    NSMutableArray *otherWords = [NSMutableArray arrayWithArray:[inKanji getRelatedWordListForJlptLevel:jlptLevel_N3 thatIsNotWord:inNotWord maxReturnCount:inMaxCount allowOutOfGradeLevel:YES]];
    
    // now get a list where meaning is unique
    NSMutableArray *uniqueList = [NSMutableArray array];
    while (uniqueList.count < inMaxCount && otherWords.count > 0) {
        // get next word
        JapaneseWord *nextWord = [otherWords objectAtIndex:0];
        
        // if word meaning is not already in list
        NSString *firstMeaning = [nextWord.englishMeanings objectAtIndex:0];
        BOOL foundDupe = NO;
        for (int i=0; i < uniqueList.count; i++) {
            JapaneseWord *uniqueWord = [uniqueList objectAtIndex:i];
            if ([[uniqueWord.englishMeanings objectAtIndex:0] isEqualToString:firstMeaning]) {
                foundDupe = YES;
                break;
            }
        }
        
        // if we didn't find dupe, add
        if (!foundDupe)
            [uniqueList addObject:nextWord];
        [otherWords removeObjectAtIndex:0];
    }
    
    return uniqueList;
}

-(KanjiRadical*)pickRadicalFromList:(NSArray*)inList thatIsNot:(KanjiRadical*)inRadical {
    // create pick list
    NSMutableArray *pickList = [NSMutableArray array];
    BOOL foundOnce = NO;
    for (int r=0; r < inList.count; r++) {
        KanjiRadical *kanjiRadical = [inList objectAtIndex:r];
        if (!foundOnce && [kanjiRadical.kanji isEqualToString:inRadical.kanji])
            foundOnce = YES;
        else
            [pickList addObject:kanjiRadical];
    }
    
    // pick a random radical if we have one
    if (pickList.count == 0)
        return nil;
    else {
        int r = pickList.count==1?0:arc4random()%pickList.count;
        return [pickList objectAtIndex:r];
    }
}

-(ConnectGameLink*)getLinkForLowestDeckObjectForJlptLevel:(jlptLevelEnum)inJlptLevel andGradeLevel:(gradeLevelEnum)inGradeLevel {
    // get list of all words for this jlpt level

    // put into an array of connect game objects
    NSMutableArray *allObjects = [NSMutableArray array];
    for (int i=0; i < allKanjis.count; i++) {
        KanjiEntry *thisKanji = [allKanjis objectAtIndex:i];
        
        // create a connect game link for each game type for this kanji
        ConnectGameObject *toObject = [[ConnectGameObject alloc] initWithDictionaryObject:thisKanji ofType:dictionaryType_Kanji];
        ConnectGameLink *thisLink = [[ConnectGameLink alloc] initWithFromObject:nil toObject:toObject viaObject:nil toGameType:gameType_GuessMeaningFromKanji];
        [allObjects addObject:thisLink];
        [toObject release];
        [thisLink release];
    }
    
    // sort using compare method based on leitner deck
    NSArray *sortedArray = [allObjects sortedArrayUsingSelector:@selector(leitnerCompare:)];
    
    // return the first item for now, but later look at randomizing those in the lowest deck
    
    return [sortedArray objectAtIndex:0];
}

-(void)markQuestionRight {
    // mark right
    [currentLink markToLinkAsAnsweredRight];
}

-(void)markQuestionWrong {
    // mark wrong
    [currentLink markToLinkAsAnsweredWrong];
}

-(BOOL)goToNextLink {
    // switch links
    [currentLink release];
    currentLink = [nextLink retain];
    [nextLink release];
    nextLink = nil;
    
    // setup question
    [self setupQuestionUsingCurrentLink];
    
    // success
    return YES;
}

-(void)dealloc {
    // release our variables
    [availableGameTypes release];
    [missingRadical release];
    
    // release super
    [super dealloc];
}


@end
