//
//  ConnectGameObject.m
//  KanjiConnect
//
//  Created by Ray Price on 4/22/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "ConnectGameObject.h"
#import "KanjiEntry.h"
#import "JapaneseWord.h"
#import "KanjiRadical.h"

@implementation ConnectGameObject

@synthesize objectType;
@synthesize dictionaryObject;
@synthesize dictionaryObjectParent;

-(id)initWithDictionaryObject:(NSObject*)inDictionaryObject ofType:(dictionaryTypeEnum)inObjectType {
    self = [super init];
    if (self) {
        // save input
        objectType = inObjectType;
        dictionaryObject = [inDictionaryObject retain];
        
        // load object
    }
    return self;
}

-(NSObject*)getDictionaryObject {
    return dictionaryObject;
}

-(int)getObjectId {
    // what type of object?
    switch (objectType) {
        case dictionaryType_Kanji:
            return ((KanjiEntry*)dictionaryObject).kanjiId;
        case dictionaryType_Word:
            return ((JapaneseWord*)dictionaryObject).dictionaryId;
        case dictionaryType_Radical:
            return ((KanjiRadical*)dictionaryObject).id;
        default:
            return 0;
    }
}

-(dictionaryTypeEnum)getObjectType {
    return objectType;
}

-(void)dealloc {
    // release our retains
    [dictionaryObject release];
    
    // release super
    [super dealloc];
}

@end
