//
//  KcLocalOptions.m
//  KanjiConnect
//
//  Created by Ray Price on 4/30/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "KcLocalOptions.h"
#import "KcAppDelegate.h"

@implementation KcLocalOptions

+(KcLocalOptions*)getSingleton {
    // get application delegate
    KcAppDelegate *appDelegate = (KcAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // return local options
    return appDelegate.localOptions;
}

-(id)init {
	if ((self = [super init])) {
    }
    return self;
}

// implement methods to add to dictionary instead of synthesizing
-(NSString*)lastVersionNumber {
    // get dictionary entry
	NSString *myLastVersionNumber = [localOptions objectForKey:@"LastVersionNumber"];
    if (myLastVersionNumber == nil)
        return @"0.0.0";
    return myLastVersionNumber;
}

-(void)setLastVersionNumber:(NSString *)inLastVersionNumber {
    // set dictionary entry
    [localOptions setObject:inLastVersionNumber forKey:@"LastVersionNumber"];
}

-(void)dealloc {
	[super dealloc];
}

@end
