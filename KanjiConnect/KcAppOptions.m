//
//  KcAppOptions.m
//  KanjiConnect
//
//  Created by Ray Price on 4/30/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import "KcAppOptions.h"
#import "KcAppDelegate.h"

@implementation KcAppOptions


+(KcAppOptions*)getSingleton {
    // get application delegate
    KcAppDelegate *appDelegate = (KcAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // return local options
    return appDelegate.appOptions;
}

-(id)initWithFilename:(NSString*)inFilename andLocalOptions:(LocalOptions*)inLocalOptions {
	if ((self = [super initWithFilename:inFilename andLocalOptions:inLocalOptions])) {
    }
    return self;
}

@end
