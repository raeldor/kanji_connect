//
//  AppDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 4/14/14.
//  Copyright (c) 2014 Ray Price. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KcAppOptions.h"
#import "KcLocalOptions.h"

@interface KcAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) KcLocalOptions *localOptions;
@property (nonatomic, retain) KcAppOptions *appOptions;
@property (nonatomic, assign) BOOL isLocalOptionsLoaded;
@property (nonatomic, assign) BOOL isOptionsLoaded;

-(void)localOptionsLoaded:(NSNotification *) notification;
-(void)appOptionsLoaded:(NSNotification *) notification;

@end
